import QtQuick 2.0
import VideoPlayerParam 1.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item {
    id: vp
    property string backImagePath: ""
    property int templateCount: 4
    onTemplateCountChanged: {

        for(var i = 0; i < videoPreviews.count; i++){
            videoPreviews.get(i).previewSurf.visible = (i < templateCount)
            var rect  = videoParam.getVideoRect(templateCount,i,vp.height)
            videoPreviews.get(i).previewSurf.x = rect.x
            videoPreviews.get(i).previewSurf.y = rect.y
            videoPreviews.get(i).previewSurf.width = rect.width
            videoPreviews.get(i).previewSurf.height = rect.height
        }
    }

    ProgressBar{
        id: prbar
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        style: RoundProgressBar{

        }
        minimumValue: 0
        maximumValue: 100
        z: 101
        visible: value === 0 || value === maximumValue ? false : true
    }

    Image{
        id: backImg
        anchors.fill: parent
        source: backImagePath === "" ? "" : "file:///" + backImagePath
        cache: false
        visible: source !== ""
    }

    ListModel{
        id: videoPreviews
        property var previewSurf
    }

    VideoParam{
        id: videoParam
    }

    Component.onCompleted: {

        for(var i = 0; i < templateCount; i++){
            var component = Qt.createComponent("VideoPreview.qml")
            if (component.status === Component.Ready) {
                var rect  = videoParam.getVideoRect(templateCount,i,vp.height)
                var childRec = component.createObject(parent)
                childRec.x = rect.x
                childRec.y = rect.y
                childRec.width = rect.width
                childRec.height = rect.height
                childRec.title = "title " + (i + 1)
                videoPreviews.append({"previewSurf":childRec})
            }
        }
    }

    function setNewTitle(index,title){
        if(index < videoPreviews.count)
            videoPreviews.get(index).previewSurf.title = title
    }
    function setNewColor(index,color){
        if(index < videoPreviews.count)
            videoPreviews.get(index).previewSurf.colortext = color
    }
    function setNewUrl(urlList){
        for(var i = 0; i < urlList.length; i++){
            videoPreviews.get(i).previewSurf.stop()
            videoPreviews.get(i).previewSurf.source = urlList[i]
            videoPreviews.get(i).previewSurf.play()
        }
    }

    function updateImage(){
        console.log("update image",backImagePath)
        backImg.source = ""
        backImg.source = backImagePath === "" ? "" : "file:///" + backImagePath
    }

    function updateProgressValue(value,maxvalue){
        console.log("update progress value",value,maxvalue)
//        prbar.visible = true
        prbar.value = value
        prbar.maximumValue = maxvalue
    }
}
