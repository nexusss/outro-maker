#ifndef BACKGROUNDIMAGE_H
#define BACKGROUNDIMAGE_H

#include <QObject>
#include <QPixmap>
#include <QImage>
#include <QPainter>

#include "defaultconstants.h"
class BackgroundImage : public QObject
{
    Q_OBJECT
    QPixmap back;

    const int deltaBetweenLogo = 10;//растояние между логотипами по вертикали и по горозинтали

    const int deltaBetweenLogoX = 50;//растояние между логотипами по горозинтали

    QVector <QImage> logoVector;
    QStringList logoPath;

    QImage subscribe;
    QString backgroundPath,projectPath;
    int templateCount;//номер шаблона

    QPoint addLogoT1(QString logoPath);
    QPoint addLogoT2(QString logoPath);
    QPoint addLogoT3(QString logoPath);
    QPoint addLogoT4(QString logoPath);

    QPoint addSubscribeLogo(QString logoPath);

    void repaintLogoT1();
    void repaintLogoT2();
    void repaintLogoT3();
    void repaintLogoT4();

    QPoint repaintSubscribeLogo();

    void repaintAddingLogo();
    void paintLogo(const int x,const int y, QImage logo,QPixmap &back);
public:
    explicit BackgroundImage(QObject *parent = 0);
    ~BackgroundImage();
    QPixmap getImg();
    QStringList getLogo();

signals:
    void updateBackImage();
public slots:
    void setProjectPath(QString projectPath);
    void addLogo(QString logoPath);
    void removeLogo(QString logoPath);
    void setBackColor(QString color);
    void setBackGradient(QString backColor, QString color);
    void setBackImage(QString path);
    void setTemplate(int templateCount);
};

#endif // BACKGROUNDIMAGE_H
