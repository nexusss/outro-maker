#include "ffmpeg_save.h"
#include <QDir>
#include <QtConcurrent/QtConcurrentRun>
#include <QTime>
#include "defaultconstants.h"
FFMPEG_Save::FFMPEG_Save(QObject *parent) : QObject(parent)
{

}

void FFMPEG_Save::init(){

    saveStarted = false;
    for(int i = 0; i < maxTemplate; i++){
        oldUrlList << "";
        oldStartTimeV << "";
        oldStopTimeV << "";
    }
}

bool FFMPEG_Save::checkDuration(QString url,QString startTime,QString stopTime){
    QProcess *process = new QProcess;

    QStringList strlist;

    strlist << "-i";
    strlist << url;

    process->start("ffmpeg.exe ",strlist);


    process->waitForFinished();
    QString line = process->readAllStandardError();

    QString durationCap;

    QRegExp rx("Duration: (\\d{2}):(\\d{2}):(\\d{2})");
    int pos = 0 ;

    while ((pos = rx.indexIn(line, pos)) != -1) {
          durationCap = rx.cap(0);

          pos += rx.matchedLength();
      }

qDebug() << "cd" << line;
    QStringList splitStr = durationCap.split(" ");
    if(splitStr.size() > 1){
        quint32 duration = QTime(0, 0, 0).secsTo(QTime::fromString(splitStr[1],"hh:mm:ss"));
        delete process;
        if((startTime.toUInt() + stopTime.toUInt()) > duration)
            return false;
    }
    else
        return false;
    return true;
}

void FFMPEG_Save::saveVideoList( QString namelist, QString urllist, QString startTimeSlist, QString durationTimeSlist, QString fileExtlist){

//    qDebug() << namelist.canConvert<QStringList>()
//             << urllist.canConvert<QStringList>()
//             << startTimeSlist.canConvert<QStringList>()
//             << durationTimeSlist.canConvert<QStringList>()
//             << fileExtlist.canConvert<QStringList>();

    qDebug() << namelist;
    qDebug() << startTimeSlist;
        QStringList name = namelist.split(',');
        qDebug() << "0";
        QStringList url = urllist.split("\r\n,");
        for (int i = 0; i < url.size();i++) {
            url[i] = url[i].replace("\r\n,","");
            url[i] = url[i].replace("\r\n","");
        }
        qDebug() << "0";
        QStringList startTimeS = startTimeSlist.split(',');
        qDebug() << "0";
        QStringList durationTimeS = durationTimeSlist.split(',');
        qDebug() << "0";
        QStringList fileExt = fileExtlist.split(',');
//    QStringList name = namelist.toStringList();
//    qDebug() << "0";
//    QStringList url = urllist.toStringList();
//    qDebug() << "0";
//    QStringList startTimeS = startTimeSlist.toStringList();
//    qDebug() << "0";
//    QStringList durationTimeS = durationTimeSlist.toStringList();
//    qDebug() << "0";
//    QStringList fileExt = fileExtlist.toStringList();
qDebug() << "0" << url.size();
qDebug() << url;
//qDebug() << startTimeS;
//qDebug()<< durationTimeS;
//qDebug()<< fileExt;
    if(saveStarted)
        return;

    QString savePath = QDir::tempPath();

    saveStarted = true;

    bool sizeOk = (url.size() == startTimeS.size()) && (startTimeS.size() == durationTimeS.size()) &&  (url.size() == name.size());

    if(sizeOk){
        savePath += "/";
        QStringList filePath;

        QFuture<void> future[maxTemplate];

        for(int i = 0; i < url.size(); i++){

            filePath << "";
            QString saveName = savePath + "title_" + QString::number(i) + "/" + name.at(i) + fileExt.at(i);

            if((oldUrlList.at(i) != url.at(i) || oldStartTimeV.at(i) != startTimeS.at(i) || oldStopTimeV.at(i) != durationTimeS.at(i)) && !url.at(i).isEmpty()){

                bool res = checkDuration(url.at(i),startTimeS.at(i),durationTimeS.at(i));
                qDebug() << "check duration " << i << res << startTimeS.at(i) << durationTimeS.at(i);
                if(!res){
                    emit error("video # " + QString::number(i + 1) + " is too short");
                    continue;
                }

                oldUrlList[i] = url.at(i);
                oldStartTimeV[i] = startTimeS.at(i);
                oldStopTimeV[i] = durationTimeS.at(i);

                QDir().mkdir(savePath + "title_" + QString::number(i));

                future[i] = QtConcurrent::run(this, &FFMPEG_Save::saveVideo,saveName,url.at(i),startTimeS.at(i),durationTimeS.at(i));

            }
            if(!url.at(i).isEmpty()){
                filePath[i] = saveName;
            }


        }
        for(int i = 0; i < url.size(); i++){
            if(future[i].isRunning())
                future[i].waitForFinished();
        }
        qDebug() << "all video saved" << filePath;
        emit allVideoSaved(QVariant::fromValue(filePath));
    }
    saveStarted = false;
}

void FFMPEG_Save::saveVideo(QString saveName, QString realUrl, QString startTimeS, QString durationTimeS){

    qDebug() << "'save video'" << realUrl;
    if(realUrl.isEmpty())
        return;

    QProcess *process = new QProcess;
    QStringList strlist;
    strlist << "-ss";
    strlist << startTimeS;
    strlist << "-t";
    strlist << durationTimeS;
    strlist << "-i";
    strlist << realUrl;
    strlist << "-c:v";
    strlist << "copy";
    strlist << "-an";
    strlist << "-y";
    strlist << saveName;

    process->start("ffmpeg.exe", strlist);


    if( !process->waitForStarted() || !process->waitForFinished(5 * 60 * 1000) ){
        return ;
    }

    process->waitForFinished(5 * 60 * 1000);

    delete process;
}

void FFMPEG_Save::addAudioToVideo(QString videoPath, QString audioPath, QString duration){

    if(videoPath.isEmpty() || audioPath.isEmpty()){
        emit addMusicFinished("");
        return;
    }

    QProcess *process = new QProcess;

    QString outputPath = QDir::currentPath() +  "/outro-video/" + QTime::currentTime().toString("hh.mm.ss") + "_template.avi";

    QString fade = "afade=t=in:ss=0:d=3,afade=t=out:st=" + QString::number(duration.toInt() - 3) + ":d=3";
    QStringList strlist;

    QFileInfo fi(audioPath);

    //fade for audio

    strlist << "-i";
    strlist << audioPath;
    strlist << "-af";
    strlist << fade;
    strlist << "-t";
    strlist << duration;
    strlist << "-y";
    strlist << QDir::tempPath() + "/audio." + fi.suffix();

    process->start("ffmpeg.exe", strlist);


    if( !process->waitForStarted() || !process->waitForFinished(5 * 60 * 1000) ){
        return ;
    }

    process->waitForFinished(5 * 60 * 1000);

    strlist.clear();

    strlist << "-i";
    strlist << QDir::tempPath() + "/audio." + fi.suffix();
    strlist << "-i";
    strlist << videoPath;
    strlist << "-t";
    strlist << duration;
    strlist << "-c:v";
    strlist << "copy";
    strlist << "-c:a";
    strlist << "copy";    
    strlist << "-y";
    strlist << outputPath;

    process->start("ffmpeg.exe", strlist);


    if( !process->waitForStarted() || !process->waitForFinished(5 * 60 * 1000) ){
        return ;
    }

    process->waitForFinished(5 * 60 * 1000);

    delete process;\

    QFile::remove(videoPath);
    QFile::remove(QDir::tempPath() + "/audio." + fi.suffix());

    emit addMusicFinished(outputPath);
}
