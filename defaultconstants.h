#ifndef DEFAULTCONSTANTS_H
#define DEFAULTCONSTANTS_H
#include <QString>

const QString defQuality = "720";
const int maxTemplate = 4;
const int defTemplate = maxTemplate;
const int arr[2] = {0,1};

const int startLogoX[maxTemplate] = {1920 - 430,
                              1920/2,
                              1920/2,
                              1920 - 810};//по горизонтали
const int startLogoY[maxTemplate] = {10,
                              1080 - 250,
                              1080 - 250,
                              230};//положение логотипа в зависимости от шаблона по вертикали
const int subscribeX[maxTemplate] = {(1920 - 430) / 2 - 800 / 2,
                              1920 / 2 - 400,
                              1920 / 2 - 400,
                              1920 - 810};
const int subscribeY[maxTemplate] = {startLogoY[1],
                              startLogoY[1] - 250,
                              startLogoY[1] - 250,
                              10};


//startLogoX[0] = 1920 - 430;
//startLogoX[1] = 250;
//startLogoX[2] = 250;
//startLogoX[3] = 1920 - 810;

//startLogoY[0] = 10;
//startLogoY[1] = 1080 - 250;
//startLogoY[2] = 1080 - 250;
//startLogoY[3] = 230;

//subscribeX[0] = (1920 - 430) / 2 - 800 / 2;
//subscribeX[1] = 1920 / 2 - 400;
//subscribeX[2] = 1920 / 2 - 400;
//subscribeX[3] = 1920 - 810;

//subscribeY[0] = startLogoY[1];
//subscribeY[1] = startLogoY[1] - 250;
//subscribeY[2] = startLogoY[1] - 250;
//subscribeY[3] = 10;
#endif // DEFAULTCONSTANTS_H
