import QtQuick 2.6
//import QtQuick.Controls 1.4
//import QtQuick.Controls.Styles 1.4
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import Qt.labs.folderlistmodel 2.1
import "InputUrl.js" as Url
import "../style"

Item {
    property string backColor: "black"
    signal setBackColor(string color)
    signal setGradientColor(string color,string color2)
    signal setBackImage(string imagePath)
    signal addLogo(string path)
    signal removeLogo(string path)
    signal toTheMainTab()
    signal qualityChanged(string value)
    signal setMusicPath(string path)

    ColorDialog {
        id: backColorDialog

        title: "Please choose background color"
        onAccepted: {
            backColor = color
            setBackColor(backColor)
        }
    }

    ColorDialog {
        id: backGardientDialog

        title: "Please choose second color"
        onAccepted: {
            setGradientColor(backColor,color)
        }
    }

    FileDialog{
        id: fileDialog
        title: "Please choose a file"
        nameFilters: [ "Image files (*.jpg *.png)" ]
        onAccepted: {
            setBackImage(fileDialog.fileUrl)
        }
    }

    Column {
        id: column1
        spacing: 5
        anchors.fill: parent

        GroupBox {
            id: groupBox1
            width: parent.width
            height: 94
            title: qsTr("")

            GridView {
                id: gridView1
                cellHeight: parent.height / 2
                cellWidth: parent.width / 3
                anchors.fill: parent

                FolderListModel {
                    id: folderModel
                    nameFilters: ["*.png"]
                    showDirs: false
                    Component.onCompleted: folder += "/logo"
                }
                model: folderModel
                delegate:
                    CheckBox {
                    id: control
                    property string fullPath: filePath
                    indicator: Rectangle {
                            implicitWidth: 26
                            implicitHeight: 26
                            x: control.leftPadding
                            y: parent.height / 2 - height / 2
                            radius: 3
                            border.color: control.down ? "#333435" : "#333435"

                            Rectangle {
                                width: 14
                                height: 14
                                x: 6
                                y: 6
                                radius: 2
                                color: control.down ? "#333435" : "#333435"
                                visible: control.checked
                            }
                        }
                    contentItem: Text{
                        color: "white"
                        text: fileName
                        horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                leftPadding: control.indicator.width + control.spacing
                    }


                    onCheckedChanged: {
                        if(checked)
                            addLogo(fullPath)
                        else
                            removeLogo(fullPath)
                    }
                }

            }
        }

        GroupBox {
            id: groupBox2
            width: parent.width
            height: 39
            title: qsTr("")

            RowLayout {
                id: row1
                x: 0
                y: 136
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.top: parent.top

                Button {
                    id: colorButton
                    text: qsTr("Color")
                    Layout.fillHeight: true
//                    style: MyButtonStyle{}
                    onClicked: backColorDialog.open()
                    contentItem: Text {
                        text: "Color"
                        color: "white"
                        horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                    }

                    background: Rectangle {
                        border.width: imageButton.down ? 2 : 1
                        border.color: "#333435"

                        radius: 4
                        color: imageButton.down? "darkgrey" : "#333435"

                    }
                }

                Button {
                    id: gradientButton
                    text: qsTr("Gradient")
                    Layout.fillHeight: true
//                    style: MyButtonStyle{}
                    onClicked: backGardientDialog.open()
                    contentItem: Text {
                        text: "Gradient"
                        color: "white"
                        horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                    }

                    background: Rectangle {
                        border.width: imageButton.down ? 2 : 1
                        border.color: "#333435"

                        radius: 4
                        color: imageButton.down? "darkgrey" : "#333435"

                    }
                }

                Button {
                    id: imageButton
                    text: qsTr("Image")
                    Layout.fillHeight: true
//                    style: MyButtonStyle{}
                    onClicked: fileDialog.open()
                    contentItem: Text {
                        text: "Image"
                        color: "white"
                        horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                    }

                    background: Rectangle {
                        border.width: imageButton.down ? 2 : 1
                        border.color: "#333435"

                        radius: 4
                        color: imageButton.down? "darkgrey" : "#333435"

                    }
                }
            }
        }

        GroupBox {
            id: groupBox3
            width: parent.width
            height: 50
            title: qsTr("")

            RowLayout {
                id: rowLayout1
                x: 0
                y: 0
                anchors.fill: parent

                Button {
                    id: musicButton
                    text: qsTr("Open music")
//                    style: MyButtonStyle{}
                    onClicked: musicFileDialog.open()
                    contentItem: Text {
                        text: "Open Music"
                        color: "white"
                        horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                    }

                    background: Rectangle {
                        border.width: imageButton.down ? 2 : 1
                        border.color: "#333435"

                        radius: 4
                        color: imageButton.down? "darkgrey" : "#333435"

                    }
                }

                Label {
                    id: musicLabel
                    color: "#ffffff"
                    text: ""
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    clip: true
                }

                Button {
                    id: closeButton
                    visible: musicLabel.text === "" ? false : true
                    text: qsTr("")
                    Layout.fillHeight: true
//                    style: MyButtonStyle{}
                    Layout.minimumHeight: image1.height
                    Layout.minimumWidth: image1.width
                    Layout.maximumHeight: image1.height
                    Layout.maximumWidth: image1.width
                    Image {
                        id: image1
                        x: 0
                        y: 0
                        width: 20
                        height: 20
                        source: "../icon/close.png"
                    }
                    onClicked: {
                        setMusicPath("")
                        musicLabel.text = ""
                    }
                }
            }
        }

        RowLayout {
            id: rowLayout2
            width: parent.width
            height: 28

            Text {
                id: text1
                text: qsTr("Video settings")
                color: "white"
                font.pixelSize: 12
            }

            ComboBox {
                id: qualityComboBox
                Layout.fillHeight: true
                model: [ "144","240","360", "480", "720", "1080" ]
                onCurrentTextChanged: qualityChanged(currentText)
                currentIndex: 4
            }

            Flickable {
                id: flickable1
                width: 300
                height: 300
                Layout.fillHeight: true
                Layout.fillWidth: true
            }

            Button {
                id: button1
                text: qsTr("Go back to the previous tab")
                Layout.fillHeight: true
//                style: MyButtonStyle{}
                onClicked: toTheMainTab()
                contentItem: Text {
                    text: "Go back to the previous tab"
                    color: "white"
                    horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                }

                background: Rectangle {
                    border.width: imageButton.down ? 2 : 1
                    border.color: "#333435"

                    radius: 4
                    color: imageButton.down? "darkgrey" : "#333435"

                }
            }
        }
    }

    FileDialog{
        id: musicFileDialog
        title: "Please choose a file"
        nameFilters: [ "Music files (*.mp3 *.ogg)" ]
        onAccepted: setNewMusicPath(fileUrl)
    }

    function setQuality(width,height){
        var index = qualityComboBox.find(height.toString())
        console.log("set qulaity outro",height,index)
        if(index >= 0)
            qualityComboBox.currentIndex = index
    }

    function setNewMusicPath(fileUrl){
//        console.log("set musc",fileUrl)
        musicLabel.text = Url.getFileName(fileUrl)
        setMusicPath(fileUrl)
    }

    function setLogo(logoName){//choose logo
//        console.log("logo",logoName)

        for(var i = 0; i < gridView1.count; i++){
            gridView1.currentIndex = i
            if(gridView1.currentItem.fullPath === logoName)
                gridView1.currentItem.checked = true

        }
    }

    function clearLogoCheck(){
        for(var i = 0; i < gridView1.count; i++){
            gridView1.currentIndex = i
            gridView1.currentItem.checked = false

        }
    }
}
