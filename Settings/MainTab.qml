import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.1
import "../style"

Item {

    id: item1
    property string quality: "720"
    property int templateCount: 4
    property variant refreshButtonEnable : [true,true,true,true]
    onTemplateCountChanged: {
        updateTemplate(templateCount)
        recalcTemplate()
    }


    signal titleChange(int index,string title)
    signal colorChange(int index, string color)
    signal startTimeChange(int index,string startTime)
    signal pathChange(int index, string path) //path in TextEdit

    signal startCacheVideo(string name,string url,string startTime,string stopTime,string fileExt);
    signal updateTemplate(int templateCount)
    signal updateDuration(int value)
    signal startRecord(string inputList,string titleList,string colorList,string quality,int templateCount)

    Rectangle{
        x: 258
        y: 66
        width: 172
        height: 16
        color: "#00000000"
        anchors.horizontalCenterOffset: 1
        anchors.horizontalCenter: parent.horizontalCenter



        ExclusiveGroup {
            id: groupBox1
        }

        RadioButton {
            id: radioButton1
            x: 0
            y: 0
            width: 87
            height: 16
            text: qsTr("Only local file")
            checked: true
            exclusiveGroup: groupBox1
            onCheckedChanged: changeInputType(checked)
            style: RadioButtonStyle{
                label: Text{
                    color: "white"
                    text: control.text
                }
            }
        }

        RadioButton {
            id: radioButton2
            x: 89
            y: 0
            width: 83
            height: 16
            text: qsTr("Youtube only")
            exclusiveGroup: groupBox1
            style: RadioButtonStyle{
                label: Text{
                    color: "white"
                    text: control.text
                }
            }

        }

    }

    ColumnLayout {
        id: columnLayout1
        anchors.top: parent.top
        anchors.topMargin: 84
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0

        ListView {
            id: inputListView
            x: 0
            y: 0
            width: parent.width
            Layout.fillWidth: true
            Layout.fillHeight: true
            delegate: InputUrl{
                id: iUrl
                objectName: "myname2"
                titleString : newTitleString
                index: newIndex
                width: parent.width
                onTitleStringChanged: titleChange(index,titleString)
                onColorTextChanged: colorChange(index,colorText)
                onStartTimeChanged: startTimeChange(index,startTime)
                onPathChanged: pathChange(index,path)

                onCanBeLoadChanged: {
                    refreshButtonEnable[index] = canBeLoad
                    checkRefreshButtonState()
                }
            }

            model: ListModel {
                id: myModel
                ListElement{
                    newTitleString : "title 1"
                    newIndex: 0
                }
                ListElement{
                    newTitleString : "title 2"
                    newIndex: 1
                }
                ListElement{
                    newTitleString : "title 3"
                    newIndex: 2
                }
                ListElement{
                    newTitleString : "title 4"
                    newIndex: 3
                }
            }
        }

        ProgressBar {
            id: progressBar
            x: 0
            y: 302
            width: 640
            height: 23
            Layout.fillWidth: true
            Layout.fillHeight: false
            visible: indeterminate
        }

        Rectangle {
            id: rectangle2
            x: 0
            y: 331
            width: 640
            height: 37
            color: "#00000000"
            Layout.fillWidth: true

            RowLayout {
                id: rowLayout1
                anchors.fill: parent
                spacing: 4

                Button {
                    id: refreshButton
                    text: qsTr("Refresh")
                    style: MyButtonStyle{}
                    enabled: {
                        return refreshButtonEnable[0] && refreshButtonEnable[1] &&
                                refreshButtonEnable[2] && refreshButtonEnable[3]
                    }

                    onClicked: {
                        progressBar.indeterminate = true
                        var names = [inputListView.count];
                        var urls = [inputListView.count];
                        var startTimes = [inputListView.count];
                        var stopTimes = [inputListView.count];
                        var fileExts = [inputListView.count];
                        for(var i = 0; i < inputListView.count; i++){
                            inputListView.currentIndex = i
                            if(inputListView.currentItem.visible){
                                names[i] = (inputListView.currentItem.fileName.toString())
                                urls[i] = (inputListView.currentItem.path.toString()) + "\r\n"
                                startTimes[i] = (inputListView.currentItem.startTime.toString())
                                stopTimes[i] = (durationSpinBox.value)
                                fileExts[i] = (inputListView.currentItem.fileExt.toString())
                            }
                            else{
                                names[i] = ("")
                                urls[i] = ("\r\n")
                                startTimes[i] = ("")
                                stopTimes[i] = ("")
                                fileExts[i] = ("")
                            }
                        }
                        startCacheVideo(names,urls,startTimes,stopTimes,fileExts)
                    }
                }

                Flickable {
                    width: 300
                    height: 300
                    contentWidth: 0
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                }

                Text {
                    text: qsTr("Outro duration")
                    font.pixelSize: 12
                    color: "white"
                }

                SpinBox {
                    id: durationSpinBox
                    value: 10
                    onValueChanged: updateDuration(value)
                }

                Button {
                    id: button6
                    text: qsTr("Add outro")
                    style: MyButtonStyle{}
                    onClicked: {

                        var urls = [inputListView.count];
                        var titles = [inputListView.count];
                        var colors = [inputListView.count];

                        for(var i = 0; i < inputListView.count; i++){
                            inputListView.currentIndex = i
                            if(inputListView.currentItem.visible){
                                urls[i] = (inputListView.currentItem.path.toString()) + "\r\n"
                                titles[i] = (inputListView.currentItem.titleString.toString()) + "\r\n"
                                colors[i] = (inputListView.currentItem.colorText.toString())
                            }
                            else{
                                urls.push("") + "\r\n"
                                titles.push("") + "\r\n"
                                colors.push("")
                            }
                        }
                        console.log("start record")
                        startRecord(urls,titles,colors,quality,templateCount)
                    }
                }
            }
        }
    }

    Rectangle {
        id: rectangle1
        x: 162
        y: 0
        width: 338
        height: 61
        color: "#00000000"
        anchors.horizontalCenter: parent.horizontalCenter

        Row {
            id: row1
            spacing: 5
            anchors.fill: parent

            Button {
                id: button1
                width: image1.width
                height: image1.height

                Image {
                    id: image1
                    width:  80
                    height: 60
                    source: "../templateImage/template1.png"
                }
                onClicked: templateCount = 1

            }

            Button {
                id: button2
                width: image1.width
                height: image1.height
                Image {
                    id: image2

                    width: 80
                    height: 60
                    source: "../templateImage/template2.png"
                }
                onClicked: templateCount = 2
            }

            Button {
                id: button3
                width: image1.width
                height: image1.height
                Image {
                    id: image3
                    width: 80
                    height: 60
                    source: "../templateImage/template3.png"
                }
                onClicked: templateCount = 3
            }

            Button {
                id: button4
                width: image1.width
                height: image1.height
                Image {
                    id: image4

                    width: 80
                    height: 60
                    source: "../templateImage/template4.png"
                }

                onClicked: templateCount = 4
            }
        }
    }

    function recalcTemplate(){
        for(var i = 0; i < inputListView.count; i++){
            inputListView.currentIndex = i
            inputListView.currentItem.visible = (i < templateCount)
        }
    }
    function changeInputType(newType){
        for(var i = 0; i < inputListView.count; i++){
            inputListView.currentIndex = i
            inputListView.currentItem.type = newType
        }
        if(newType === 1)
            clearPath()
    }
    function checkRefreshButtonState(){
        refreshButton.enabled = refreshButtonEnable[0] && refreshButtonEnable[1] &&
                refreshButtonEnable[2] && refreshButtonEnable[3];
    }

    function cacheIsOver(){
        progressBar.indeterminate = false

    }

    function setVideo(index,path,title,color,startTime){
        inputListView.currentIndex = index
        if(path.toString().indexOf("http") !== -1)
            radioButton2.checked = true
        else
            radioButton1.checked = true

        inputListView.currentItem.setPathfiledText(path)
        inputListView.currentItem.titleString = title
        inputListView.currentItem.colorText = color
        inputListView.currentItem.startTime = startTime
    }

    function setDuration(duration){
        durationSpinBox.value = duration
    }

    function clearPath(){
        for(var i = 0; i < inputListView.count; i++){
            inputListView.currentIndex = i
            inputListView.currentItem.setPathfiledText("")
        }
    }
}
