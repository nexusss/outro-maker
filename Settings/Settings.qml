import QtQuick 2.0
//import QtQuick.Controls 1.4
//import Qt.labs.controls 1.0
import QtQuick.Controls 2.0


Rectangle {

    color: "#222222"
    property int templateCount: mainTab.templateCount
    signal ntitleChange(int index,string title)
    signal ncolorChange(int index,string color)
    SwipeView {
        id: swipeView
        width: parent.width
        height: parent.height - 20
        currentIndex: tabBar.currentIndex
        activeFocusOnTab: true
        MainTab{
            id: mainTab
            objectName: "mainTab"
            y: 0
            x: 0
            height: swipeView.height
            width: swipeView.width
            onTitleChange: ntitleChange(index,title)
            onColorChange: ncolorChange(index,color)

        }
        SettingsTab{
            id: settingsTab
            objectName: "settingsTab"
            y: 0
            x: swipeView.width
            height: swipeView.height
            width: swipeView.width
            onToTheMainTab: {
                tabBar.currentIndex = 0
            }
            onQualityChanged: {
                mainTab.quality = value
            }
        }
    }
    TabBar {
        x:0
        y:swipeView.height
        height: 20
        width: parent.width
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            height: parent.height
            text: qsTr("Main")
        }
        TabButton {
            height: parent.height
            text: qsTr("Settings")

        }
    }
}
