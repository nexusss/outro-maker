import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import "InputUrl.js" as Url
import YoutubeAction 1.0
import "../style"

Item {
    property string colorText: "white"
    property bool type: true
    property string path: "" //real path to video
    property string fileExt: "" //file extension
    property string fileName : "" // filename
    property string tmppath: "" //path to youtube video, like https://www.youtube.com/watch?v=TsI_00OasDCM

    property string titleString: ""
    property int index: 0
    property bool canBeLoad: true
    property string startTime: "0"
    property string textInput: pathFiled.text

    height: 25
    onTypeChanged: {
        if(type === false)
            tmppath = path
        else
            tmppath = ""
        checkUrlCorrection()
    }

    ColorDialog {
        id: colorDialog

        title: "Please choose a color"
        onAccepted: {colorText = colorDialog.color

        }
    }

    MessageDialog {
         id: messageDialog
         text: "url # " + (index + 1) + " not correct"
     }

    FileDialog{
        id: fileDialog
        title: "Please choose a file"
        nameFilters: [ "Video files (*.avi *.mp4)" ]
        onAccepted: {
            pathFiled.text = fileUrl


        }
    }

    Youtubeaction{
        id: iu
        onRealUrlWasFound: {
            path = realUrl
            Url.urlGeted = true
            canBeLoad = Url.urlGeted && Url.extGeted
        }
        onFileExtWasFound: {

            fileExt = youtubeFileExt
            Url.extGeted = true
            canBeLoad = Url.urlGeted && Url.extGeted
        }
    }

    RowLayout {
        id: rowLayout1
        anchors.fill: parent

        Button {
            id: openbutton
            text: qsTr("Open file")
            visible: type
            onClicked: fileDialog.open()
            style: MyButtonStyle{}
        }

        TextField {
            id: textField1
            y: 0
            text: titleString
            placeholderText: qsTr("Title")
            onTextChanged: titleString = text
        }

        Image{
            id: colorRect
            source: "../icon/color_wheel.png"
            width: sourceSize.width
            height: sourceSize.height

            Layout.minimumHeight: 6
            Layout.minimumWidth: 16
            opacity: 1

            Layout.fillHeight: true
            Layout.fillWidth: false

            MouseArea {
                anchors.fill: parent
                onClicked: colorDialog.open()

            }

        }

        TextField {
            id: pathFiled
            y: 0
            readOnly: type
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            Layout.fillWidth: true

            font.pixelSize: 12

            onTextChanged: {
                if(type === true ){
                    path = text.toString().replace("file:///","")
                    var fullFileName =  Url.getFileName(path)
                    fileName = fullFileName.toString().split('.')[0]
                    fileExt = "." + fullFileName.toString().split('.').pop();
                }
                else{
                    tmppath = text
                    fileName = "youtube"
                }
                checkUrlCorrection()

            }
        }

        TextField {
            id: startTimeFiled
            y: 0
            text: startTime
            Layout.maximumWidth: 23
            placeholderText: qsTr("")
            validator: IntValidator {bottom: 0; }
            onTextChanged: {
                if(!text) text = "0"
                startTime = text
            }
        }
    }
    function checkUrlCorrection(){
        var ok = false
        if(!type)
            ok = Url.checkYoutubeCorrection(tmppath)
        else
            ok = true
        if(!ok){
            messageDialog.open()
        }
        else if(type === false){
            if(tmppath){
                Url.urlGeted = false
                Url.extGeted = false
                canBeLoad = false
                iu.getRealUrl(tmppath)
                iu.getFileExt(tmppath)
            }
            else{
                path = ""
                fileExt = ""
            }
        }
    }
    function setPathfiledText(text){
        pathFiled.text = text
    }
}
