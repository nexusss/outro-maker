
var urlGeted = false
var extGeted = false

function checkYoutubeCorrection(url) {
    if(!url)
        return true;

    var matches = url.match(/watch\?v=([a-zA-Z0-9\-_]+)/) ;
    var urlOk = (matches !== null)

        urlOk &= (url.indexOf("youtube") !== -1) || (url.indexOf("youtu.be") !== -1)


    return urlOk

}


function getFileName(path){
    if (path) {
        var startIndex = (path.toString().indexOf('\\') >= 0 ? path.toString().lastIndexOf('\\') : path.toString().lastIndexOf('/'));
        var filename = path.toString().substring(startIndex);
        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
            filename = filename.substring(1);

        }
        return filename
    }
    return ""
}
