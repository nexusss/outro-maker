import QtQuick 2.0
import QtQuick.Controls.Styles 1.4
ButtonStyle {

            background: Rectangle {
                border.width: control.activeFocus ? 2 : 1
                border.color: "#333435"

                radius: 4
                gradient: Gradient {
                    GradientStop { position: 0 ; color: control.pressed ? "darkgrey" : "#333435" }
                    GradientStop { position: 1 ; color: control.pressed ? "darkgrey" : "#333435" }
                }

            }

            label: Text {
                color: "white"
                text: control.text
            }
        }
