import QtQuick 2.5
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

ProgressBarStyle
{
   panel : Rectangle
   {
      color: "transparent"
      implicitWidth: 200
      implicitHeight: implicitWidth

      Rectangle
      {
         id: outerRing
         z: 0
         anchors.fill: parent
         radius: Math.max(width, height) / 2
         color: "transparent"
         border.color: "gray"
         width: 20
      }

      Rectangle
      {
         id: innerRing
         z: 1
         anchors.fill: parent
         anchors.margins: (outerRing.border.width - border.width) / 2
         radius: outerRing.radius
         color: "transparent"
         border.color: "darkgray"
         border.width: 4

         ConicalGradient
         {
            source: innerRing
            anchors.fill: parent

            gradient: Gradient
            {

               GradientStop { position: 0.00; color: "royalblue" }
               GradientStop { position: (control.value - control.minimumValue) / (control.maximumValue - control.minimumValue) ; color: "royalblue" }
               GradientStop { position:  (((control.value - control.minimumValue) / (control.maximumValue - control.minimumValue)) + 0.01) ; color: "transparent" }
            }
         }
      }

      Text
      {
         id: progressLabel
         anchors.centerIn: parent
         color: "black"
         text: (((control.value - control.minimumValue) / (control.maximumValue - control.minimumValue)) * 100).toFixed() + "%"
      }
   }
}
