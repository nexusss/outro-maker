TEMPLATE = app

QT += qml quick
CONFIG += c++11

SOURCES += main.cpp \
    videoplayerparam.cpp \
    ffmpeg_save.cpp \
    outromaker.cpp \
    backgroundimage.cpp \
    templaterenderer.cpp \
    ../filedownloader.cpp \
    ../youtubeAction.cpp

RESOURCES += \
    outroqml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)
LIBS += -lgdi32
LIBS += -lUser32

#INCLUDEPATH += $$PWD/include
#DEPENDPATH += $$PWD/include

#win32:!win32-g++: PRE_TARGETDEPS += $$PWD/x86/vc12/lib/opencv_world300.lib
#else:win32-g++: PRE_TARGETDEPS += $$PWD/x86/vc12/lib/libopencv_world300.a
include(../QmlVlc/QmlVlc.pri)

DISTFILES +=

HEADERS += \
    videoplayerparam.h \
    ffmpeg_save.h \
    outromaker.h \
    backgroundimage.h \
    templaterenderer.h \
    ../filedownloader.h \
    ../youtubeAction.h

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/x86/vc12/lib/ -lopencv_world300
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/x86/vc12/lib/ -lopencv_world300d

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/x86/vc12/lib/libopencv_world300.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/x86/vc12/lib/libopencv_world300d.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/x86/vc12/lib/opencv_world300.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/x86/vc12/lib/opencv_world300d.lib
