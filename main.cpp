#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <../QmlVlc.h>
#include <../QmlVlc/QmlVlcConfig.h>
#include <QThread>
#include "videoplayerparam.h"
#include "../youtubeAction.h"
#include "outromaker.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<VideoPlayerParam>("VideoPlayerParam", 1, 0, "VideoParam");
    qmlRegisterType<YoutubeAction>("YoutubeAction", 1, 0, "Youtubeaction");

    RegisterQmlVlc();
    QmlVlcConfig& config = QmlVlcConfig::instance();
    config.enableAdjustFilter( true );
    config.enableMarqueeFilter( true );
    config.enableLogoFilter( true );
    config.enableDebug( false );

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/outromakerqml/main.qml")));

    Outromaker outromaker(engine,0);
//    InputUrl *iu = new InputUrl(engine);

    return app.exec();
}
