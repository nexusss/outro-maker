#ifndef OUTROMAKER_H
#define OUTROMAKER_H

#include <QObject>
#include <QQuickView>
#include <QQuickItem>
//#include <QQmlApplicationEngine>
#include <QThread>
#include "ffmpeg_save.h"
#include "templaterenderer.h"
#include "backgroundimage.h"

struct OutroSettings{
    QStringList path;
    QString templateNumber;
    QString quality;
    QStringList logoChecked;
    QStringList title;
    QStringList color;
    QString musicPath;
    QStringList startTime;
    int duration;//
};

class Outromaker : public QObject
{
    Q_OBJECT
//    QObject *rootObject;
    BackgroundImage *bImg;
    QObject *mainTab = 0;
    QObject *settingsTab = 0;
    QObject *vp = 0;
    QThread ffmpeg_saveThread;
    FFMPEG_Save *ffmpeg_save;

    TemplateRenderer *templateRenderer;
    QThread renderThread;

    bool recordMode = false;
    QString musicPath = "";
    int duration = 10;

    QStringList fileNames;
    void setFileNames(QVariant fn);

    OutroSettings os;
    QString projectPath = "";
    QString projectName = "";
public:
//    explicit Outromaker(QQmlApplicationEngine &engine, QObject *parent = 0);
    ~Outromaker();
    explicit Outromaker(QQuickView *engine,QObject *parent = 0);
    OutroSettings getOutroSettings();
    void loadNewOutroSettings(OutroSettings outroSetings);
signals:
    void startCapture(std::vector<QString> fileNames,   /* file names of videos to place in subframes */
                          QString outFile,                  /* output video file */
                          QSize outSize,                    /* output size, e.g. QSize(1920, 1280) */
                          std::vector<QRect> videoRects,    /* coordinates and size of subframes */
                          QString canvasFile,               /* background image file */
                          uint msDuration,          /* output video duration in milliseconds */
                          uint frameRate);
    void recordIsOver(QString path);
public slots:
    void setProjectPath(QString projectPath);
    void setProjectName(QString projectName);

    void startRecord(QString inputList, QString titleList, QString colorList, QString quality, int templateCount);
    void stopCapture(QString path);
    void setMusicPath(QString path);
    void setDuration(int duration);
    void setTitle(int index,QString title);
    void setColor(int index,QString color);
    void setPath(int index,QString path);
    void setStartTime(int index,QString startTime);
};

#endif // OUTROMAKER_H
