#include "backgroundimage.h"
#include <QDebug>
#include <QDir>

void saveBackground(QPixmap back){
    back.save(QDir::tempPath() + "/background.png");
}

inline void calcLogoPosT1(int &currStartY,const int &height,const int &delta){
    currStartY += height + delta;
}

void calcLogoPosT2(QVector <int> &currStartX,int startPos,const QVector <int> &width,const int &delta){

    const double numLogo = width.size() / 2.0;
    int maxLeft = 0;
    for(int i = 0, j = 0; i < numLogo * 2;i++, j += 2){
        maxLeft += (width.at(j / 2) / 2);

    }



    maxLeft += delta * numLogo;

    startPos -= maxLeft;

    for(int i = 0; i < width.size(); i++){

        currStartX.push_back(startPos);
        startPos += width.at(i) + delta;
    }

}

void calcLogoPosT3(QVector <int> &currStartX,int startPos,const QVector <int> &width,const int &delta){

    const double numLogo = width.size() / 2.0;
    int maxLeft = 0;
    for(int i = 0, j = 0; i < numLogo * 2;i++, j += 2){
        maxLeft += (width.at(j / 2) / 2);

    }
    maxLeft += delta * numLogo;
    startPos -= maxLeft;

    for(int i = 0; i < width.size(); i++){

        currStartX.push_back(startPos);
        startPos += width.at(i) + delta;
    }

}

void calcLogoPosT4(int &currStartX,int &currStartY,const int &width,const int &deltaX,const int &height,const int &delta,const int defX){
    currStartX += width + deltaX;
    if(currStartX + width >= 1920){
        currStartY += height + delta;
        currStartX = defX;
    }
}

void BackgroundImage::paintLogo(const int x,const int y, QImage logo,QPixmap &back){//нарисовать логотип
    QPainter p(&back);
    p.drawImage(x,y,logo);
    p.end();


    saveBackground(back);
}

BackgroundImage::BackgroundImage(QObject *parent) : QObject(parent)
{
    templateCount = 4;

    back = QPixmap(1920,1080);
    QPainter p(&back);
    p.fillRect(back.rect(),QBrush(QColor("black")));
    p.end();
}

void BackgroundImage::setProjectPath(QString projectPath){
    if(projectPath.isEmpty())
        return;

    QDir backDir;
    backDir.mkdir(projectPath + "/background");

    backgroundPath = projectPath + "/background/background.png";
    QPixmap tmpImg = QPixmap(backgroundPath);
    if(tmpImg.isNull()){
        back.save(backgroundPath);
    }
    else{
        back = tmpImg.scaled(1920,1080);
    }
    saveBackground(back);
    emit updateBackImage();
}

QPixmap BackgroundImage::getImg(){
    return back;
}

QStringList BackgroundImage::getLogo(){
    return logoPath;
}

QPoint BackgroundImage::addLogoT1(QString logoPath){
    int currStartY = startLogoY[0];



    foreach (QImage img, logoVector)
        calcLogoPosT1(currStartY,img.height(),deltaBetweenLogo);



    QImage logo = QImage(logoPath);
    if(logo.width() < 500)
        logoVector.push_back(logo);
    else{
        float prop = 500 / logo.width();
        logoVector.push_back(logo.scaled(logo.width() * prop,logo.height() * prop));
    }

    paintLogo(startLogoX[0],currStartY,logoVector.last(),back);
    return QPoint(startLogoX[0],currStartY);
}

QPoint BackgroundImage::addLogoT2(QString logoPath){
    int currStartX = startLogoX[1];
    QPixmap tmpImg = QPixmap(backgroundPath);
    back = tmpImg.scaled(1920,1080);

//    foreach (QImage img, logoVector)
//        calcLogoPosT2(currStartX,img.width(),deltaBetweenLogoX);

    QImage logo = QImage(logoPath);
    if(logo.height() < 210)
        logoVector.push_back(logo);
    else{
        float prop = 210 / logo.width();
        logoVector.push_back(logo.scaled(logo.width() * prop,logo.height() * prop));
    }


//    paintLogo(currStartX,startLogoY[1],logoVector.last(),back);
    repaintLogoT2();
    repaintSubscribeLogo();
    return QPoint(currStartX,startLogoY[1]);
}

QPoint BackgroundImage::addLogoT3(QString logoPath){
    int currStartX = startLogoX[2];


    QPixmap tmpImg = QPixmap(backgroundPath);
    back = tmpImg.scaled(1920,1080);

//    foreach (QImage img, logoVector)
//        calcLogoPosT2(currStartX,img.width(),deltaBetweenLogoX);

    QImage logo = QImage(logoPath);
    if(logo.height() < 210)
        logoVector.push_back(logo);
    else{
        float prop = 210 / logo.width();
        logoVector.push_back(logo.scaled(logo.width() * prop,logo.height() * prop));
    }


//    paintLogo(currStartX,startLogoY[1],logoVector.last(),back);
    repaintLogoT2();
    repaintSubscribeLogo();
    return QPoint(currStartX,startLogoY[2]);
}

QPoint BackgroundImage::addLogoT4(QString logoPath){
    int currStartX = startLogoX[3];
    int currStartY = startLogoY[3];

    foreach (QImage img, logoVector){
        calcLogoPosT4(currStartX,currStartY,img.width(),
                      deltaBetweenLogoX,img.height(),
                      deltaBetweenLogo,startLogoX[3]);
    }

    QImage logo = QImage( logoPath);
    if(logo.height() < 210)
        logoVector.push_back(logo);
    else{
        float prop = 210 / logo.width();
        logoVector.push_back(logo.scaled(logo.width() * prop,logo.height() * prop));
    }

    paintLogo(currStartX,currStartY,logoVector.last(),back);

    return QPoint(currStartX,currStartY);
}

QPoint BackgroundImage::addSubscribeLogo(QString logoPath){
    subscribe = QImage(logoPath);
    return repaintSubscribeLogo();

}

void BackgroundImage::addLogo(QString logoPath){
    qDebug() << "add logo" << logoPath;
    this->logoPath << logoPath;
    QPoint p;
    switch (templateCount) {
    case 1:
        if(!logoPath.contains("subscribe800.png"))
            p = addLogoT1(logoPath);
        break;
    case 2:
        if(!logoPath.contains("subscribe800.png"))
            p = addLogoT2(logoPath);
        break;
    case 3:
        if(!logoPath.contains("subscribe800.png"))
            p = addLogoT3(logoPath);
        break;
    case 4:
        if(!logoPath.contains("subscribe800.png"))
            p = addLogoT4(logoPath);
        break;
    }

    if(logoPath.contains("subscribe800.png"))
        p = addSubscribeLogo(logoPath);

    emit updateBackImage();
//    return p;
}

void BackgroundImage::repaintLogoT1(){
    int currStartY = startLogoY[0];



    foreach (QImage img, logoVector){
        paintLogo(startLogoX[0],currStartY,img,back);
        calcLogoPosT1(currStartY,img.height(),deltaBetweenLogo);
    }
}

void BackgroundImage::repaintLogoT2(){
    int currStartX = startLogoX[1];


    QVector <int> width;
    QVector <int> pos;
    foreach (QImage img, logoVector){
        width.push_back(img.width());
    }



    calcLogoPosT2(pos,currStartX,width,deltaBetweenLogoX);

    for (int i = 0; i < logoVector.size(); i++){

        paintLogo(pos.at(i),startLogoY[1],logoVector.at(i),back);

    }
}

void BackgroundImage::repaintLogoT3(){
    int currStartX = startLogoX[2];

    QVector <int> width;
        QVector <int> pos;
        foreach (QImage img, logoVector){
            width.push_back(img.width());
        }



        calcLogoPosT2(pos,currStartX,width,deltaBetweenLogoX);

        for (int i = 0; i < logoVector.size(); i++){
            paintLogo(pos.at(i),startLogoY[2],logoVector.at(i),back);

        }
}

void BackgroundImage::repaintLogoT4(){
    int currStartX = startLogoX[3];
    int currStartY = startLogoY[3];

    foreach (QImage img, logoVector){
        paintLogo(currStartX,currStartY,img,back);

        calcLogoPosT4(currStartX,currStartY,img.width(),
                      deltaBetweenLogoX,img.height(),
                      deltaBetweenLogo,startLogoX[3]);
    }
}

QPoint BackgroundImage::repaintSubscribeLogo(){

    if (!subscribe.isNull()){
        paintLogo(subscribeX[templateCount - 1],subscribeY[templateCount - 1],subscribe,back);
    }

    return QPoint(subscribeX[templateCount - 1],subscribeY[templateCount - 1]);
}

void BackgroundImage::repaintAddingLogo(){
    QPixmap tmpImg = QPixmap(backgroundPath);
    back = tmpImg.scaled(1920,1080);

    switch (templateCount) {
    case 1:
            repaintLogoT1();
        break;
    case 2:
            repaintLogoT2();
        break;
    case 3:
            repaintLogoT3();
        break;
    case 4:
            repaintLogoT4();
        break;
    }
    repaintSubscribeLogo();
    saveBackground(back);
    emit updateBackImage();
}

void BackgroundImage::removeLogo(QString logoPath){

    logoVector.removeOne(QImage(logoPath));
    this->logoPath.removeOne(logoPath);

    QPixmap tmpImg = QPixmap(backgroundPath);
    back = tmpImg.scaled(1920,1080);

    if(logoPath.contains("subscribe800.png"))
        subscribe = QImage();

    repaintAddingLogo();
}

void BackgroundImage::setBackColor(QString color){
    QPixmap tmpImg = QPixmap(backgroundPath);
    back = tmpImg.scaled(1920,1080);
    QPainter p(&back);
    p.fillRect(back.rect(),QBrush(QColor(color)));

    p.end();
    back.save(backgroundPath);
//    saveBackground(back);
    repaintAddingLogo();
}

void BackgroundImage::setBackGradient(QString backColor, QString color){
    QGradient gradient = QLinearGradient(776,982,776,128);
    gradient.setColorAt(0,color);
    gradient.setColorAt(1,backColor);

    QPixmap tmpImg = QPixmap(backgroundPath);
    back = tmpImg.scaled(1920,1080);


    QPainter p(&back);
    p.fillRect(back.rect(),gradient);

    p.end();

    back.save(backgroundPath);
//    saveBackground(back);
    repaintAddingLogo();
}

void BackgroundImage::setBackImage(QString path){
    path.remove("file:///");
    QPixmap newBack(path);
    back = newBack.scaled(1920,1080);

    back.save(backgroundPath);
//    saveBackground(back);
    repaintAddingLogo();
}

void BackgroundImage::setTemplate(int t){
    this->templateCount = t;
    repaintAddingLogo();
}

BackgroundImage::~BackgroundImage(){
    QFile::remove(QDir::tempPath() + "/background.png");
}
