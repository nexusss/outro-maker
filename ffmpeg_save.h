#ifndef FFMPEG_SAVE_H
#define FFMPEG_SAVE_H

#include <QObject>
#include <QStringList>
#include <QProcess>
#include <QByteArray>
#include <QVariant>

class FFMPEG_Save : public QObject
{
    Q_OBJECT

    QStringList oldUrlList;
    QStringList oldStartTimeV;
    QStringList oldStopTimeV;

    bool saveStarted;

    FILE* ffmpeg;

//    bool checkCorrectUrl(const QString &url, QString &youtubeId);
    void saveVideo(QString saveName, QString url, QString startTimeS, QString stopTimeS);

    void readOutput();
    bool checkDuration(QString url,QString startTime,QString stopTime);
public:
    void init();
    explicit FFMPEG_Save(QObject *parent = 0);



signals:
    void allVideoSaved(QVariant filePath);
    void error(QString str);
    void addMusicFinished(QString path);
public slots:
    void saveVideoList(QString namelist, QString urllist, QString startTimeSlist, QString stopTimeSlist, QString fileExtlist);
    void addAudioToVideo(QString videoPath,QString audioPath,QString duration);
};

#endif // FFMPEG_SAVE_H
