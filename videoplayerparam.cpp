#include "videoplayerparam.h"


void getVideoPosT1(const int video,const int quality,QRect &rect);
void getVideoPosT2(const int video,const int quality,QRect &rect);
void getVideoPosT3(const int video,const int quality,QRect &rect);
void getVideoPosT4(const int video,const int quality,QRect &rect);

VideoPlayerParam::VideoPlayerParam(QObject *parent) : QObject(parent)
{

}

QRect VideoPlayerParam::getVideoRect(int templateCount,int videoNumber,int quality){
    QRect videoRect;

    switch (templateCount) {
    case 1:
        getVideoPosT1(videoNumber,quality,videoRect);
        break;
    case 2:
        getVideoPosT2(videoNumber,quality,videoRect);
        break;
    case 3:
        getVideoPosT3(videoNumber,quality,videoRect);
        break;
    case 4:
        getVideoPosT4(videoNumber,quality,videoRect);
        break;
    }

    return videoRect;
}

int getFontSize(const int quality){
    double prop = quality / 1080.0;
    return 100 * prop;
}

void getVideoPosT1(const int video,const int quality,QRect &rect){
    int x,y,w,h;
    switch(video){
        case 0:
    {
        double prop = quality / 1080.0;
        const int width1080 = 1036;
        const int height1080 = width1080 * 9 / 16;

        w =  width1080 * prop;
        h =  height1080 * prop;

        const int x1080 = (1920 - 430) / 2 - width1080 / 2;
        const int y1080 = 10;
        x = x1080 * prop;
        y = y1080 * prop;
}
        break;

    }
    rect = QRect(x,y,w,h);
}
void getVideoPosT2(const int video,const int quality,QRect &rect){
    int x,y,w,h;
    const int delta = 100;
    double prop = quality / 1080.0;
    const int width1080 = (1920 - delta *3) / 2;
    const int height1080 = width1080 * 9 / 16;

    w =  width1080 * prop;
    h =  height1080 * prop;

    switch(video){
        case 0:
        {

        const int x1080 = delta;
        const int y1080 = 10;
        x = x1080 * prop;
        y = y1080 * prop;
        }
        break;

        case 1:
        {

        const int x1080 = delta * 2 + width1080;
        const int y1080 = 10;
        x = x1080 * prop;
        y = y1080 * prop;

        }
        break;


    }
    rect = QRect(x,y,w,h);
}
void getVideoPosT3(const int video,const int quality,QRect &rect){
    int x,y,w,h;
    const int delta = 70;
    double prop = quality / 1080.0;
    const int width1080 = (1920 - delta * 4) / 3;
    const int height1080 = width1080 * 9 / 16;
    w =  width1080 * prop;
    h =  height1080 * prop;


    switch(video){
        case 0:
        {
        const int x1080 = delta;
        const int y1080 = 10;
        x = x1080 * prop;
        y = y1080 * prop;
        }
        break;

        case 1:
        {
        const int x1080 = delta * 2 + width1080;
        const int y1080 = 10;
        x = x1080 * prop;
        y = y1080 * prop;
        }
        break;

        case 2:
        {
        const int x1080 = delta * 3 + 2 *width1080;
        const int y1080 = 10;
        x = x1080 * prop;
        y = y1080 * prop;
        }
        break;


    }
    rect = QRect(x,y,w,h);
}

void getVideoPosT4(const int video,const int quality,QRect &rect){
    int x,y,w,h;
    double prop = quality / 1080.0;
    const int delta = 40;
    const int widthMax1080 = 853;
    const int heightMax1080 = widthMax1080 * 9 / 16;

    const int widthMin1080 = (1920 - delta *4) / 3;
    const int heightMin1080 = widthMin1080 * 9 / 16;


    switch(video){
        case 0:
        {

        const int x1080 = delta;
        const int y1080 = 10;
        w =  widthMax1080 * prop;
        h =  heightMax1080 * prop;
        x = x1080 * prop;
        y = y1080 * prop;
            }
        break;

        case 1:
        {
        w =  widthMin1080 * prop;
        h =  heightMin1080 * prop;
        const int x1080 = delta;
        const int y1080 = 1080 - getFontSize(1080) - heightMin1080;
        x = x1080 * prop;
        y = y1080 * prop;
        }

        break;

        case 2:
        {
            w =  widthMin1080 * prop;
            h =  heightMin1080 * prop;
            const int x1080 = delta * 2 + widthMin1080;
            const int y1080 = 1080 - getFontSize(1080) - heightMin1080;
            x = x1080 * prop;
            y = y1080 * prop;
        }
        break;

        case 3:
        {
            w =  widthMin1080 * prop;
            h =  heightMin1080 * prop;
            const int x1080 = delta * 3 + 2 * widthMin1080;
            const int y1080 = 1080 - getFontSize(1080) - heightMin1080;
            x = x1080 * prop;
            y = y1080 * prop;
        }
        break;
    }

    rect = QRect(x,y,w,h);
}
