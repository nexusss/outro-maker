#ifndef TEMPLATERENDERER_H
#define TEMPLATERENDERER_H

#include <QObject>
#include <QRect>
#include <QStringList>
#include <QVariant>
#include <opencv2/opencv.hpp>

#include <vector>
#include <memory>



class TemplateRenderer : public QObject
{
    Q_OBJECT
public:
    explicit TemplateRenderer(QObject *parent = 0);

    void capture (std::vector<QString> fileNames,   /* file names of videos to place in subframes */
                  QString outFile,                  /* output video file */
                  QSize outSize,                    /* output size, e.g. QSize(1920, 1280) */
                  std::vector<QRect> videoRects,    /* coordinates and size of subframes */
                  QString canvasFile,               /* background image file */
                  uint msDuration = 10000,          /* output video duration in milliseconds */
                  uint frameRate = 30);             /* frame rate */



signals:
    void progressValue(QVariant progress,QVariant maxValue);
    void finished(QString videoPath);

};

#endif // TEMPLATERENDERER_H
