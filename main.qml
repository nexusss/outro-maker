import QtQuick 2.6
//import QtQuick.Window 2.2
import "Settings"
Rectangle {
//Window{
    id: rectangle2
    visible: true
    width: 420
    height:541
    color: "#222222"

    VideoPlayer {
        id: videoPlayer
        objectName: "videoPlayer"
        x: 0
        y: 0
        width: 420
        height: 239
        anchors.horizontalCenter: parent.horizontalCenter
        templateCount: settings.templateCount
    }


    Settings{
       id: settings
       x: 0
       y: 255
       height: 286
       width: parent.width

       onNtitleChange: videoPlayer.setNewTitle(index,title)
       onNcolorChange: videoPlayer.setNewColor(index,color)
    }

    Rectangle {
        id: rectangle1
        x: 0
        y: 239
        width: 420
        height: 19
        color: "#000000"
    }
}
