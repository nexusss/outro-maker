#include "templaterenderer.h"
#include <QDebug>
#include <QtConcurrent/QtConcurrent>
using namespace std;

TemplateRenderer::TemplateRenderer(QObject *parent) : QObject(parent)
{

}



void TemplateRenderer::capture(std::vector<QString> fileNames,
                               QString outFile, QSize outSize,
                               std::vector<QRect> videoRects,
                               QString canvasFile,
                               uint msDuration, uint frameRate)
{
    qDebug() << "start capture" << fileNames << outFile << outSize << videoRects << canvasFile << msDuration << frameRate;

    cv::VideoWriter writer(outFile.toStdString(),
                           CV_FOURCC('D', 'I', 'V', 'X'),
                           frameRate,
                           cv::Size(outSize.width(), outSize.height()),
                           true);

    qDebug() << "0";
    vector<shared_ptr<cv::VideoCapture>> captures;
    qDebug() << "1" << fileNames.size();
    bool exit = false;
    for (auto video: fileNames) {
        auto capture = shared_ptr<cv::VideoCapture>(new cv::VideoCapture(video.toStdString()));
        captures.push_back(capture);
    }
    cv::Mat canvas = cv::imread(canvasFile.toStdString(), CV_LOAD_IMAGE_COLOR);
    /* If no canvas image, create a black canvas */
    if (canvas.empty()) {

        canvas = cv::Mat::zeros(outSize.height(), outSize.width(), CV_8UC3);
    }
//    qDebug() << "sc 1";
    /* Resizing canvas to match the output size */
    cv::resize(canvas, canvas, cv::Size(outSize.width(), outSize.height()));

    const uint numberOfFrames = msDuration / 1000 * frameRate;
    const int oneframeDuration = 1000 / frameRate;
    for (uint i = 0; i < numberOfFrames && !exit; ++i) {

        cv::Mat frame = canvas;
        for (size_t j = 0; j < captures.size(); ++j) {
            cv::Mat subFrame;
            if (!captures.at(j)->isOpened()) {

                continue;
            }
            if (!captures.at(j)->read(subFrame)) {
                exit = true;
                emit progressValue(QVariant::fromValue(msDuration),QVariant::fromValue(msDuration));
                continue;
            }
            QRect videoRect = videoRects.at(j);
            cv::Rect rect (videoRect.x(), videoRect.y(), videoRect.width(), videoRect.height());
            /* Check whether input video rectangle is within the canvas */
            Q_ASSERT ((rect & cv::Rect(0, 0, frame.cols, frame.rows)) == rect);
            cv::resize(subFrame, subFrame, cv::Size(rect.width, rect.height));
            subFrame.copyTo(frame(rect));

        }
        emit progressValue(QVariant::fromValue(i * oneframeDuration),QVariant::fromValue(msDuration));
        writer.write(frame);
    }
    qDebug() << "end";
    emit progressValue(QVariant::fromValue(msDuration),QVariant::fromValue(msDuration));
    writer.release();
    qDebug() << "write release";
    emit finished(outFile);

}
