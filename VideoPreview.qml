import QtQuick 2.5
import QmlVlc 0.1
import QtMultimedia 5.0
import QtQuick.Layouts 1.3

Item {
    property string title: ""
    property string colortext: "white"
    property string source: ""

    onSourceChanged: {
        console.log("source",source,source === "",title)
    }

    onVisibleChanged: {
        if(visible === false)
            vlcPlayer.stop()
    }

    function play(){
        if(visible)
        vlcPlayer.play()
    }
    function stop(){
        vlcPlayer.stop()
    }

    VlcPlayer {
        id: vlcPlayer
        mrl: source === "" ? "" : "file:///" + source
//        onMediaPlayerOpening: stop()

        onMediaPlayerEndReached: play()
    }


    ColumnLayout{
        anchors.fill: parent
        VlcVideoSurface {
//        Rectangle{

            id: vlcSurface
            width: parent.width
            height: parent.height
            Layout.fillHeight: true
            Layout.fillWidth: true
            source: vlcPlayer
//            color: "red"
        }
        Text {
            id: name
            text: source === "" ? "" : title
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.fillWidth: true
            color: colortext
        }
    }
}
