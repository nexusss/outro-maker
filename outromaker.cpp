#include "outromaker.h"
#include <QDir>
#include <QTime>

#include "videoplayerparam.h"
#include <QDebug>

//Outromaker::Outromaker(QQmlApplicationEngine &engine, QObject *parent) : QObject(parent)
//{
//    QObject *rootObject = engine.rootObjects().at(0);

Outromaker::Outromaker(QQuickView *engine, QObject *parent) : QObject(parent)
{
    QObject *rootObject = engine->rootObject();

    mainTab = rootObject->findChild<QObject*>("mainTab");
    settingsTab = rootObject->findChild<QObject*>("settingsTab");
    vp = rootObject->findChild<QObject*>("videoPlayer");


    ffmpeg_save = new FFMPEG_Save();
    ffmpeg_save->moveToThread(&ffmpeg_saveThread);

    templateRenderer = new TemplateRenderer();
    templateRenderer->moveToThread(&renderThread);
    connect(&renderThread,&QThread::destroyed,templateRenderer,&TemplateRenderer::deleteLater);
    connect(templateRenderer,&TemplateRenderer::finished,this,&Outromaker::stopCapture);
    connect(templateRenderer,SIGNAL(progressValue(QVariant,QVariant)),vp,SLOT(updateProgressValue(QVariant,QVariant)));

    bImg = new BackgroundImage(this);

    vp->setProperty("backImagePath",QDir::tempPath() + "/background.png");

    connect(&ffmpeg_saveThread,&QThread::started,ffmpeg_save,&FFMPEG_Save::init);
    connect(&ffmpeg_saveThread,&QThread::finished,ffmpeg_save,&FFMPEG_Save::deleteLater);
    connect(mainTab,SIGNAL(startCacheVideo(QString,QString,QString,QString,QString)),ffmpeg_save,SLOT(saveVideoList(QString,QString,QString,QString,QString)));
    connect(mainTab,SIGNAL(updateTemplate(int)),bImg,SLOT(setTemplate(int)));
    connect(mainTab,SIGNAL(updateDuration(int)),this,SLOT(setDuration(int)));
    connect(mainTab,SIGNAL(titleChange(int,QString)),this,SLOT(setTitle(int,QString)));
    connect(mainTab,SIGNAL(colorChange(int,QString)),this,SLOT(setColor(int,QString)));
    connect(mainTab,SIGNAL(startTimeChange(int,QString)),this,SLOT(setStartTime(int,QString)));
    connect(mainTab,SIGNAL(pathChange(int,QString)),this,SLOT(setPath(int,QString)));
    connect(mainTab,SIGNAL(startRecord(QString,QString,QString,QString,int)),this,SLOT(startRecord(QString,QString,QString,QString,int)));

    connect(ffmpeg_save,SIGNAL(allVideoSaved(QVariant)),vp,SLOT(setNewUrl(QVariant)));
    connect(ffmpeg_save,&FFMPEG_Save::allVideoSaved,this,&Outromaker::setFileNames);
    connect(ffmpeg_save,SIGNAL(allVideoSaved(QVariant)),mainTab,SLOT(cacheIsOver()));
    connect(ffmpeg_save,SIGNAL(addMusicFinished(QString)),this,SIGNAL(recordIsOver(QString)));
    connect(settingsTab,SIGNAL(setBackColor(QString)),bImg,SLOT(setBackColor(QString)));
    connect(settingsTab,SIGNAL(setGradientColor(QString,QString)),bImg,SLOT(setBackGradient(QString,QString)));
    connect(settingsTab,SIGNAL(setBackImage(QString)),bImg,SLOT(setBackImage(QString)));
    connect(settingsTab,SIGNAL(addLogo(QString)),bImg,SLOT(addLogo(QString)));
    connect(settingsTab,SIGNAL(removeLogo(QString)),bImg,SLOT(removeLogo(QString)));
    connect(settingsTab,SIGNAL(setMusicPath(QString)),this,SLOT(setMusicPath(QString)));
    connect(bImg,SIGNAL(updateBackImage()),vp,SLOT(updateImage()));

    qRegisterMetaType<std::vector<QString> >("std::vector<QString>");
    qRegisterMetaType<std::vector<QRect> >("std::vector<QRect>");
    connect(this,&Outromaker::startCapture,templateRenderer,&TemplateRenderer::capture);

    ffmpeg_saveThread.start();
    renderThread.start();
    os.duration = 10;
    os.quality = "720";
    for(int i = 0; i < 4;i++){
        os.title << "Title " + QString::number(i + 1);
        os.color << "black";
        os.path << "";
        os.startTime << "0";
    }
}

void Outromaker::loadNewOutroSettings(OutroSettings outroSetings){

    QMetaObject::invokeMethod(settingsTab, "setNewMusicPath", Qt::QueuedConnection,
                                Q_ARG(QVariant, QVariant::fromValue(outroSetings.musicPath)));

    foreach (QString str, outroSetings.logoChecked) {
        QMetaObject::invokeMethod(settingsTab, "setLogo", Qt::QueuedConnection,
                                    Q_ARG(QVariant, QVariant::fromValue(str)));
    }

    mainTab->setProperty("templateCount",QVariant::fromValue(outroSetings.templateNumber));
    int minSize = outroSetings.color.size();
    if(outroSetings.title.size() < minSize)
        minSize = outroSetings.title.size();
    if(outroSetings.path.size() < minSize)
        minSize = outroSetings.path.size();
    if(outroSetings.startTime.size() < minSize)
        minSize = outroSetings.startTime.size();
    for(int i = 0; i < minSize; i++){
        QString path = outroSetings.path.at(i) == "" ? "" : "file:///" + outroSetings.path.at(i);
        QMetaObject::invokeMethod(mainTab, "setVideo", Qt::QueuedConnection,
                                    Q_ARG(QVariant, QVariant::fromValue(i)),
                                    Q_ARG(QVariant, QVariant::fromValue(path)),
                                    Q_ARG(QVariant, QVariant::fromValue(outroSetings.title.at(i))),
                                    Q_ARG(QVariant, QVariant::fromValue(outroSetings.color.at(i))),
                                    Q_ARG(QVariant, QVariant::fromValue(outroSetings.startTime.at(i))));
    }
    QMetaObject::invokeMethod(mainTab, "setDuration", Qt::QueuedConnection,
                                Q_ARG(QVariant, QVariant::fromValue(outroSetings.duration)));

}

void Outromaker::setProjectPath(QString projectPath){
    this->projectPath = projectPath;
    bImg->setProjectPath(projectPath);
    QMetaObject::invokeMethod(mainTab, "clearPath", Qt::QueuedConnection);

    QMetaObject::invokeMethod(settingsTab, "clearLogoCheck", Qt::QueuedConnection);


}

void Outromaker::setProjectName(QString projectName){
    this->projectName = projectName;
}

OutroSettings Outromaker::getOutroSettings(){

    os.logoChecked = bImg->getLogo();
    os.templateNumber = mainTab->property("templateCount").toString();
    os.quality = mainTab->property("quality").toString();
    return os;
}

void Outromaker::stopCapture(QString path){

    if(musicPath.isEmpty()){
        emit recordIsOver(path);
    }
    else{
        QMetaObject::invokeMethod(ffmpeg_save, "addAudioToVideo", Qt::QueuedConnection,
                                   Q_ARG(QString, path),
                                   Q_ARG(QString, musicPath),
                                   Q_ARG(QString, QString::number(duration)));
    }
    recordMode = false;
}

void Outromaker::setMusicPath(QString path){
    musicPath = path;
    os.musicPath = path;
}

void Outromaker::setDuration(int duration){
    qDebug() << "outro duration" << duration;
    this->duration = duration;
    os.duration = duration;
}

void Outromaker::setFileNames(QVariant fn){
    fileNames = fn.toStringList();

}

void Outromaker::setTitle(int index,QString title){
    if(index < os.title.size())
        os.title[index] = title;
}

void Outromaker::setColor(int index,QString color){
    if(index < os.color.size())
        os.color[index] = color;
}

void Outromaker::setPath(int index,QString path){
    if(index < os.path.size())
        os.path[index] = path;
}

void Outromaker::setStartTime(int index,QString startTime){
    if(index < os.startTime.size())
        os.startTime[index] = startTime;
}

void Outromaker::startRecord(QString inputList,QString titleList,QString colorList,QString quality,int templateCount){

    if(recordMode)
        return;

    QString canvas ;

    QVector <QRect> rects;
    QSize size;
    QVector <QString> input;// = fileNames.toVector();
    QStringList il = fileNames;
    QStringList tl = titleList.split("\r\n,");
    QStringList cl = colorList.split(",");

    bool listIsEmty = true;
    foreach (QString str, fileNames) {
        listIsEmty &= str.isEmpty();
    }

    qDebug() << "listIS empty" << listIsEmty;
    if(listIsEmty)
        return;


    for(int i = 0; i < tl.size(); i++){
        tl[i] = tl[i].replace("\r\n,","");
        tl[i] = tl[i].replace("\r\n","");
    }

    for(int i = 0; i < templateCount;i++)
        input.push_back(il.at(i));

    canvas = QDir::tempPath() + "/background.png";
    QImage img = QImage(canvas);
    QImage img2 = img.scaled(quality.toInt() * 16/9,quality.toInt());

    QPainter p(&img2);


    if(quality == "1080"){
        p.setFont(QFont("Verdana",24));
    }
    else if(quality == "720"){
        p.setFont(QFont("Verdana",20));
    }
    else if(quality == "480"){
        p.setFont(QFont("Verdana",16));
    }
    else if(quality == "360"){
        p.setFont(QFont("Verdana",14));
    }
    else if(quality == "240"){
        p.setFont(QFont("Verdana",12));
    }
    else if(quality == "144"){
        p.setFont(QFont("Verdana",10));
    }

    VideoPlayerParam vpp;

    for(int i = 0; i < templateCount; i++){
        rects.push_back(vpp.getVideoRect(templateCount,i,quality.toInt()));


        if(!il[i].isEmpty()){
            QPen penHText(cl.at(i));
            p.setPen(penHText);
            p.drawText(rects.last().x(),
                       rects.last().y() + rects.last().height(),
                       rects.last().width(),
                       getFontSize(quality.toInt()),
                       Qt::AlignHCenter,tl.at(i));
        }
    }
    p.end();
    img2.save(QDir::tempPath() + "/backgroundRecord.png");

    size = img2.size();

    QDir dir;
    dir.mkdir("outro-video");
    QString outFile = QDir::currentPath() + "/outro-video/" + QTime::currentTime().toString("hh.mm.ss") +"_template.avi";

    uint frameRate = 24;

    uint msDuration = duration * 1000;

    recordMode = true;
//    itw->disableButton(false);
//    if(backIsVideo)
//        emit startCapture(input.toStdVector(),outFile,size,rects.toStdVector(),"",msDuration,frameRate);
//    else
        emit startCapture(input.toStdVector(),outFile,size,rects.toStdVector(),QDir::tempPath() + "/backgroundRecord.png",msDuration,frameRate);

}

Outromaker::~Outromaker(){
    ffmpeg_saveThread.quit();
    renderThread.quit();
    ffmpeg_saveThread.wait();
    renderThread.wait();
}
