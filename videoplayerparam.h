#ifndef VIDEOPLAYERPARAM_H
#define VIDEOPLAYERPARAM_H

#include <QObject>
#include <QRect>
int getFontSize(const int quality);
class VideoPlayerParam : public QObject
{
    Q_OBJECT
public:
    explicit VideoPlayerParam(QObject *parent = 0);
    Q_INVOKABLE QRect getVideoRect(int templateCount, int videoNumber, int quality);
signals:

public slots:
};

#endif // VIDEOPLAYERPARAM_H
