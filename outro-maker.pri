QT += qml quick
CONFIG += c++11

SOURCES +=  \
    $$PWD/videoplayerparam.cpp \
    $$PWD/ffmpeg_save.cpp \
#    $$PWD/inputurlaction.cpp \
    $$PWD/outromaker.cpp \
    $$PWD/backgroundimage.cpp \
    $$PWD/templaterenderer.cpp

RESOURCES += \
    $$PWD/outroqml.qrc

# Default rules for deployment.
include(deployment.pri)
LIBS += -lgdi32
LIBS += -lUser32

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/x86/vc12/lib/opencv_world300.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/x86/vc12/lib/libopencv_world300.a
include(../QmlVlc/QmlVlc.pri)


HEADERS += \
    $$PWD/videoplayerparam.h \
    $$PWD/ffmpeg_save.h \
#    $$PWD/inputurlaction.h \
    $$PWD/outromaker.h \
    $$PWD/backgroundimage.h \
    $$PWD/templaterenderer.h


